﻿using System;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using BarcodeLib;
using Domain;

namespace Barcode
{
    public class BarcodeGenerator
    {
        private static string GetPathToSave(string folder)
        {
            try
            {
                var prepend = string.IsNullOrEmpty(folder) ? string.Empty : $"{folder}_";
                var monthDay = DateTime.Today.ToString("yyyy-MMMM-dd");
                var root = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                var dir = Path.Combine(root, $"Barcodes\\{prepend}{monthDay}\\");
                var file = new FileInfo(dir);
                file.Directory?.Create();
                return dir;
            }
            catch (Exception e)
            {
                //throw new Exception(e.Message);
                Console.WriteLine(e.Message);
                Debug.WriteLine(e.Message);
            }
            return null;
        }

        private static TYPE GetLibraryBarcodeLibType(BarcodeType encodeType)
        {
            var value2 = (TYPE)Enum.Parse(typeof(TYPE), encodeType.ToString());
            return value2;
        }

        public void MakeBarcode(BarcodeModel model)
        {
            var barcodeType = GetLibraryBarcodeLibType(model.GetEncodeType());
            var encodeString = model.GetEncodeString();
            var filename = model.GetBarcodeFilename();

            var b = new BarcodeLib.Barcode
            {
                IncludeLabel = model.ShowLabel,
                LabelPosition = LabelPositions.BOTTOMCENTER,
                BackColor = model.Background,
                ForeColor = model.Foreground,
                Width = 540,
                Height = 240,
                ImageFormat = ImageFormat.Png
            };
            try
            {
                var img = b.Encode(barcodeType, encodeString);
                var pathToSave = GetPathToSave(model.FolderName);
                var path = $"{pathToSave}{filename}-{barcodeType.ToString().ToUpperInvariant()}.png";
                using (var memory = new MemoryStream())
                {
                    using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
                    {
                        img.Save(memory, b.ImageFormat);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
            }
            catch (Exception e)
            {
                //throw new Exception(e.Message);
                Console.WriteLine(e.Message);
                Debug.WriteLine(e.Message);
            }
        }

        public void MakeMultipleBarcodes(BarcodeFormModel model)
        {
            
            try
            {
                foreach (var b in model.Barcodes)
                {
                    var barcode = new BarcodeModel
                    {
                        FolderName = model.FolderName,
                        StringToEncode = b,
                        ShowLabel = model.ShowLabel,
                        Background = model.Background,
                        Foreground = model.Foreground
                    };
                    MakeBarcode(barcode);
                }
            }
            catch (Exception e)
            {
                //throw new Exception(e.Message);
                Console.WriteLine(e.Message);
                Debug.WriteLine(e.Message);
            }
        }
    }
}
