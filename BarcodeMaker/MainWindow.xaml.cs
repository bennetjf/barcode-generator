﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using Barcode;
using BarcodeMaker.Models.ViewModels;
using Domain;
using Microsoft.Win32;

namespace BarcodeMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        BackgroundWorker _mOWorker;
        BarcodeFormModel _formModel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BarcodeTypes_Loaded(object sender, RoutedEventArgs e)
        {
            var list = new List<BarcodeTypeListItemModel>();
            var tuple = new BarcodeTypesTuple();
            var theList = tuple.GetList();
            foreach (var item in theList)
            {
                var barcodeTypeInfo = new BarcodeTypeListItemModel
                {
                    Title = item.Item2,
                    Description = item.Item3,
                    ImageLocation = item.Item4
                };
                list.Add(barcodeTypeInfo);
            }
            EncodeTypeCollection.ItemsSource = list;
            EncodeTypeCollection.SelectedIndex = 0;
        }

        private void Go_OnClick(object sender, RoutedEventArgs e)
        {
            Cancel.IsEnabled = true;
            _mOWorker = new BackgroundWorker();

            _mOWorker.DoWork += m_oWorker_DoWork;
            _mOWorker.ProgressChanged += m_oWorker_ProgressChanged;
            _mOWorker.RunWorkerCompleted += m_oWorker_RunWorkerCompleted;
            _mOWorker.WorkerReportsProgress = true;
            _mOWorker.WorkerSupportsCancellation = true;

            try
            {
                var tuple = new BarcodeTypesTuple();
                var theList = tuple.GetList();

                var selectedBarcodeType = theList[EncodeTypeCollection.SelectedIndex].Item1;
                // check if numbers are valid
                _formModel = new BarcodeFormModel
                {
                    FolderName = prefix.Text,
                    EncodeType = selectedBarcodeType,
                    ShowLabel = chkShowLabel.IsChecked != null && chkShowLabel.IsChecked.Value,
                    Barcodes = (from object entry in ListBarcodes.Items select entry.ToString()).ToList()
                };

            }
            catch (Exception)
            {
            }
        }

        private void Cancel_OnClick(object sender, EventArgs e)
        {
            if (_mOWorker.IsBusy)
            {
                _mOWorker.CancelAsync();
            }
        }

        private void ImportCsv_OnClick(object sender, RoutedEventArgs e)
        {
            ListBarcodes.Items.Clear();
            try
            {
                var openFileDialog = new OpenFileDialog {Filter = "CSV files (*.csv)|*.csv"};
                if (openFileDialog.ShowDialog() != true) return;

                var strContents = File.ReadAllText(openFileDialog.FileName);
                var listCodes = Regex.Split(Regex.Replace(strContents, "^[,\r\n]+|[,\r\n]+$", ""), "[,\r\n]+");
                foreach (var code in listCodes)
                {
                    ListBarcodes.Items.Add(code);
                }

                //validate entries here maybe
            }
            catch (Exception)
            { 
            }
        }

        private void m_oWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var barcodeGeneratorService = new BarcodeGenerator();
            var counted = _formModel.Barcodes.Count;
            var counter = 0;

            foreach (var b in _formModel.Barcodes)
            {
                counter++;
                var barcode = new BarcodeModel
                {
                    FolderName = _formModel.FolderName,
                    StringToEncode = b,
                    ShowLabel = _formModel.ShowLabel
                };
                barcodeGeneratorService.MakeBarcode(barcode);
                _mOWorker.ReportProgress(counter/counted*100);

                if (!_mOWorker.CancellationPending) continue;
                e.Cancel = true;
                _mOWorker.ReportProgress(0);
                return;
            }

            Cancel.IsEnabled = false;
            _mOWorker.ReportProgress(100);
        }

        private void m_oWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressBarMain.Value = e.ProgressPercentage;
        }

        private void m_oWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                ProgressBarMain.Value = 0;
            }
            else if (e.Error != null)
            {
                ProgressBarMain.Value = 0;
            }
            else
            {
                ProgressBarMain.Value = 75;
            }
        }
    }
}
