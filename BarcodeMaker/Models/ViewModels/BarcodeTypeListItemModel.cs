﻿namespace BarcodeMaker.Models.ViewModels
{
    public class BarcodeTypeListItemModel
    {
        public string Title { get; set; }
        public string ImageLocation { get; set; }
        public string Description { get; set; }
    }
}
