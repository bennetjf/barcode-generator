﻿using System;
using System.Linq;
using Domain;

namespace BarcodeGenerator
{
    internal class Program
    {
        private static void Main()
        {
            var booklandExample = new BarcodeModel
            {
                StringToEncode = "978456123"
            };
            var b = new Barcode.BarcodeGenerator();


            var allTypes = Enum.GetValues(typeof(BarcodeType)).Cast<BarcodeType>();
            foreach (var bt in allTypes)
            {
                Console.Write(bt.ToString());
                Console.Write(" ==> ");
                booklandExample.EncodeType = bt;
                b.MakeBarcode(booklandExample);
                Console.WriteLine("OK");
            }
        }

    }
}


/*
 * TYPE.BOOKLAND : Must start with 978 and be length must be 9, 10, 12, 13 characters.
 * TYPE.CODE11   : Numeric data only, tested with 30 char length
 *
 */
