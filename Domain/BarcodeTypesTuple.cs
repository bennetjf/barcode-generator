﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class BarcodeTypesTuple
    {
        public List<Tuple<BarcodeType, string, string, string>> GetList()
        {
            const string path = "/Resources/Samples/";
            return new List<Tuple<BarcodeType, string, string, string>>
            {
                Tuple.Create(BarcodeType.BOOKLAND, "Bookland", "This is the book land type :-)", $"{path}978456123-BOOKLAND.png"),
                Tuple.Create(BarcodeType.ISBN, "ISBN", "This is the ISBN type :-)", $"{path}978456123-ISBN.png"),
                Tuple.Create(BarcodeType.PostNet, "PostNet", "This is the PostNet type :-)", $"{path}978456123-POSTNET.png"),
            };
        }
    }
}
