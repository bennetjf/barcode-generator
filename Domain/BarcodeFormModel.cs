﻿using System.Collections.Generic;

namespace Domain
{
    public class BarcodeFormModel : BarcodeModel
    {
        public List<string> Barcodes { get; set; }
    }
}
