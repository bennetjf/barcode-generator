﻿using System.Drawing;

namespace Domain
{
    public class BarcodeModel
    {
        public BarcodeModel()
        {
            ShowLabel = true;
            Foreground = Color.Black;
            Background = Color.White;
        }

        public string FolderName { get; set; }
        public string StringToEncode { get; set; }
        public BarcodeType EncodeType { get; set; }
        public bool ShowLabel { get; set; }
        public Color Foreground { get; set; }
        public Color Background { get; set; }

        public BarcodeType GetEncodeType()
        {
            return EncodeType;
        }

        public string GetEncodeString()
        {
            return StringToEncode;
        }

        public string GetBarcodeFilename()
        {
            return StringToEncode;
        }
    }
}
